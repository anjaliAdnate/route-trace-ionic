import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FuelConsumptionReportPage } from './fuel-consumption-report';
import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    FuelConsumptionReportPage,
  ],
  imports: [
    IonicPageModule.forChild(FuelConsumptionReportPage),
    SelectSearchableModule
  ],
})
export class FuelConsumptionReportPageModule {}
