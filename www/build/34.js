webpackJsonp([34],{

/***/ 1022:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FuelConsumptionReportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FuelConsumptionReportPage = /** @class */ (function () {
    function FuelConsumptionReportPage(navCtrl, navParams, apiCall, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.portstemp = [];
        this._listData = [];
        this.fuelDataArr = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
    }
    FuelConsumptionReportPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FuelConsumptionReportPage');
    };
    FuelConsumptionReportPage.prototype.ngOnInit = function () {
        this.getdevices();
    };
    FuelConsumptionReportPage.prototype.getId = function (veh) {
        debugger;
        this.vehId = veh.Device_ID;
    };
    FuelConsumptionReportPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apiCall.startLoading().present();
        this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.portstemp = data.devices;
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    FuelConsumptionReportPage.prototype.getReport = function () {
        var _this = this;
        if (this.vehId == undefined) {
            var toast = this.toastCtrl.create({
                message: "Please Select Device ... ",
                duration: 1500,
                position: "bottom"
            });
            toast.present();
        }
        else {
            var _baseURL = "https://www.oneqlik.in/notifs/detFLRep?t=" + new Date(this.datetimeEnd).toISOString() + "&f=" + new Date(this.datetimeStart).toISOString() + "&i=" + this.vehId;
            this.apiCall.startLoading().present();
            this.apiCall.getSOSReportAPI(_baseURL)
                .subscribe(function (res) {
                _this.apiCall.stopLoading();
                // console.log(data)
                _this.fuelDataArr = [];
                if (res.result.length > 2) {
                    var add1 = res.result[1].address ? res.result[1].address.split(",") : "";
                    var addr1 = "";
                    for (var j = 2; j < (add1.length - 1); j++) {
                        add1[j] = add1[j];
                        addr1 = (addr1 ? addr1 : "") + (add1[j] ? add1[j] : "");
                    }
                    var obj = {
                        "date_time": "At the start of the  period",
                        "tank_surplus": res.result[0].currentFuel ? (res.result[0].currentFuel).toFixed(3) : "",
                        "location": addr1 ? addr1 : ""
                    };
                    _this.fuelDataArr.push(obj);
                    var totalFuelIn = 0.0;
                    var totalDuelOut = 0.0;
                    var totalFuelUsed = 0.0;
                    var totalDistance = 0.0;
                    for (var i = 1; i < (res.result.length - 1); i++) {
                        // console.log("in fu");
                        var tank_surplus_bef;
                        var fuel;
                        if (res.result[i].pour == 'OUT') {
                            tank_surplus_bef = (res.result[i].litres + res.result[i].currentFuel).toFixed(3);
                            fuel = "-" + res.result[i].litres;
                        }
                        else if (res.result[i].pour == 'IN') {
                            tank_surplus_bef = (res.result[i].currentFuel - res.result[i].litres).toFixed(3);
                            fuel = res.result[i].litres;
                        }
                        var t1 = __WEBPACK_IMPORTED_MODULE_3_moment__(res.result[i].timestamp).format('DD/MM/YYYY, h:mm:ss a');
                        var arr = t1.split(",");
                        var add = res.result[i].address ? res.result[i].address.split(",") : "";
                        var addr = "";
                        for (var j = 2; j < (add.length - 1); j++) {
                            add[j] = add[j];
                            addr = addr ? addr : "" + add[j] ? add[j] : "";
                        }
                        _this.eventT = res.result[i].pour;
                        var tanksurplus = 0;
                        if (res.result[i].currentFuel != 0) {
                            tanksurplus = res.result[i].currentFuel;
                        }
                        var p_obj = {
                            "date_time": arr[0] + "," + arr[1],
                            "drive_time": _this.secondsToHms((new Date(res.result[i].timestamp).getTime() - new Date(res.result[i - 1].timestamp).getTime()) / 1000),
                            "tank_surplus_before": tank_surplus_bef,
                            "fuel_used": (res.result[i - 1].currentFuel - tank_surplus_bef).toFixed(3),
                            "fuel_change": fuel,
                            "tank_surplus": tanksurplus.toFixed(3),
                            "distance": ((res.result[i].odo - res.result[i - 1].odo) != NaN) ? (res.result[i].odo - res.result[i - 1].odo).toFixed(3) : "N/A",
                            "event_type": res.result[i].pour,
                            "location": addr ? addr : "N/A",
                        };
                        // console.log("inside value of distance", p_obj.distance);
                        totalFuelUsed = totalFuelUsed + Number((res.result[i - 1].currentFuel - tank_surplus_bef).toFixed(3));
                        totalDistance = totalDistance + Number(p_obj.distance);
                        if (res.result[i].pour == 'OUT') {
                            totalFuelIn = totalFuelIn + res.result[i].litres;
                        }
                        if (res.result[i].pour == 'IN') {
                            totalDuelOut = totalDuelOut + res.result[i].litres;
                        }
                        _this.fuelDataArr.push(p_obj);
                        // console.log(p_obj);
                    }
                    var len = res.result.length;
                    var add2 = res.result[len - 2].address ? res.result[len - 2].address.split(",") : "";
                    var addr2 = "";
                    for (var j = 2; j < (add2.length - 1); j++) {
                        add2[j] = add2[j];
                        addr2 = (addr2 ? addr2 : "") + (add2[j] ? add2[j] : "");
                    }
                    var obj1 = {
                        "date_time": "At the end  of the period",
                        "drive_time": _this.secondsToHms((new Date(res.result[len - 1].timestamp).getTime() - new Date(res.result[len - 2].timestamp).getTime()) / 1000),
                        "tank_surplus": (res.result[len - 1].currentFuel) ? (res.result[len - 1].currentFuel).toFixed(3) : '',
                        "distance": ((res.result[len - 1].odo - res.result[len - 2].odo) != NaN) ? (res.result[len - 1].odo - res.result[len - 2].odo).toFixed(3) : "NA",
                        "location": addr2 ? addr2 : addr2
                    };
                    _this.fuelDataArr.push(obj1);
                    var obj2 = {
                        "date_time": "Total",
                        "fuel_used": totalFuelUsed.toFixed(3),
                        "fuel_change": '-' + totalFuelIn.toFixed(3) + '  ,' + totalDuelOut.toFixed(3),
                        "distance": (totalDistance != NaN) ? totalDistance.toFixed(3) : "NA"
                    };
                    _this.fuelDataArr.push(obj2);
                    console.log(_this.fuelDataArr);
                }
                // if (data.message == "Success") {
                //   this._listData = data.result;
                // }
            }, function (err) {
                console.log(err);
                _this.apiCall.stopLoading();
            });
        }
    };
    FuelConsumptionReportPage.prototype.secondsToHms = function (d) {
        d = Number(d);
        var h = Math.floor(d / 3600);
        var m = Math.floor(d % 3600 / 60);
        var s = Math.floor(d % 3600 % 60);
        var hDisplay = h > 0 ? h + (h == 1 ? " hr, " : " hrs, ") : "";
        var mDisplay = m > 0 ? m + (m == 1 ? " min, " : " mins, ") : "";
        var sDisplay = s > 0 ? s + (s == 1 ? " sec" : " sec") : "";
        return hDisplay + mDisplay + sDisplay;
    };
    FuelConsumptionReportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-fuel-consumption-report',template:/*ion-inline-start:"/Users/admin/Desktop/ONGOING_PROJECTS/IONIC_APPS/route-trace-ionic/src/pages/fuel-consumption-report/fuel-consumption-report.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Fuel Consumption Report</ion-title>\n  </ion-navbar>\n  <ion-item style="background-color: #fafafa;">\n    <ion-label style="margin-top: 15px;">Select Vehicle</ion-label>\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name"\n      itemTextField="Device_Name" [canSearch]="true" (onChange)="getId(selectedVehicle)">\n    </select-searchable>\n  </ion-item>\n  <ion-row padding-left padding-right style="background-color: #fafafa;">\n    <ion-col width-20>\n      <ion-label>\n        <span style="font-size: 13px">From Date</span>\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeStart"\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;">\n        </ion-datetime>\n      </ion-label>\n    </ion-col>\n\n    <ion-col width-20>\n      <ion-label>\n        <span style="font-size: 13px">To Date</span>\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd"\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;">\n        </ion-datetime>\n      </ion-label>\n    </ion-col>\n\n    <ion-col width-20>\n      <div style="margin-top: 9px; float: right;">\n        <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="getReport();"></ion-icon>\n      </div>\n    </ion-col>\n  </ion-row>\n</ion-header>\n<ion-content>\n  <ion-card *ngFor="let r of fuelDataArr; let i = index;" [ngClass]="(i == 0) ? \'my1\':\'my2\'">\n    <ion-row>\n      <!-- <div *ngIf="i == 0"> -->\n        <ion-col col-12>\n          <ion-row>\n            <ion-col col-6 style="font-size:10px; ">Date & Time</ion-col>\n            <ion-col col-6 style="font-size:10px; ">Tank Surplus</ion-col>\n          </ion-row>\n        </ion-col>\n        <ion-col col-12>\n          <ion-row>\n            <ion-col col-6 style="font-size:11px; font-weight: bold;">{{r.date_time}}</ion-col>\n            <ion-col col-6 style="font-size:11px; font-weight: bold;">{{r.tank_surplus}}</ion-col>\n          </ion-row>\n        </ion-col>\n      <!-- </div> -->\n    </ion-row>\n  </ion-card>\n  <ion-card *ngFor="let r of fuelDataArr; let i = index;" [ngClass]="(r.event_type==\'IN\')?\'my-class1\':\'my-class2\'">\n    <div *ngIf="(i != 0) && (i != (fuelDataArr.length - 2)) && (i != (fuelDataArr.length - 1))">\n      <ion-item\n        style="padding-left: 19px; color: white; font-size: 12px; font-weight: bold; background-color: transparent;">\n        <ion-label item-start>\n          <ion-icon name="calendar" color="light"></ion-icon>&nbsp;{{r.date_time}}\n        </ion-label>\n        <ion-badge item-end color="light">{{r.event_type}}</ion-badge>\n      </ion-item>\n\n      <ion-card-content style="background-color: transparent;">\n        <ion-row style="font-size: 10px">\n          <ion-col col-4>Drive Time</ion-col>\n          <ion-col col-4>Fuel Used(L)</ion-col>\n          <ion-col col-4>Tank Surplus Before</ion-col>\n        </ion-row>\n        <ion-row style="font-size: 11px; font-weight: bold;">\n          <ion-col col-4>{{r.drive_time}}</ion-col>\n          <ion-col col-4>{{r.fuel_used}}</ion-col>\n          <ion-col col-4>{{r.tank_surplus_before}}</ion-col>\n        </ion-row>\n        <ion-row style="font-size: 10px">\n          <ion-col col-4>Fuel Change</ion-col>\n          <ion-col col-4>Tank Surplus</ion-col>\n          <ion-col col-4>Distance(Kms)</ion-col>\n        </ion-row>\n        <ion-row style="font-size: 11px; font-weight: bold;">\n          <ion-col col-4>{{r.fuel_change}}</ion-col>\n          <ion-col col-4>{{r.tank_surplus}}</ion-col>\n          <ion-col col-4>{{r.distance}}</ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-1>\n            <ion-icon name="pin" color="secondary"></ion-icon>\n          </ion-col>\n          <ion-col col-11>\n            {{r.location}}\n          </ion-col>\n        </ion-row>\n      </ion-card-content>\n    </div>\n  </ion-card>\n  <ion-card *ngFor="let r of fuelDataArr; let i = index;" [ngClass]="(i == (fuelDataArr.length - 2)) ? \'my1\':\'my2\'">\n    <!-- <div *ngIf="i == (fuelDataArr.length - 2)"> -->\n      <ion-row>\n        <ion-col col-12 style="font-size:10px; ">Date & Time</ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6 style="font-size:11px; font-weight: bold;">{{r.date_time}}</ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6 style="font-size:10px;">Distance(Kms)</ion-col>\n        <ion-col col-6 style="font-size:10px; ">Tank Surplus</ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6 style="font-size:11px; font-weight: bold;">{{r.distance}}</ion-col>\n        <ion-col col-6 style="font-size:11px; font-weight: bold;">{{r.tank_surplus}}</ion-col>\n      </ion-row>\n    <!-- </div> -->\n  </ion-card>\n  <ion-card *ngFor="let r of fuelDataArr; let i = index;" [ngClass]="(i == (fuelDataArr.length - 1)) ? \'my1\':\'my2\'">\n    <!-- <div *ngIf="i == (fuelDataArr.length - 1)"> -->\n      <ion-row>\n        <ion-col col-12 style="font-size:13px; text-align: center; ">Total</ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-4 style="font-size:10px;">Fuel Used(L)</ion-col>\n        <ion-col col-5 style="font-size:10px; ">Fuel Change</ion-col>\n        <ion-col col-3 style="font-size:10px; ">Distance(Kms)</ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-4 style="font-size:11px; font-weight: bold;">{{r.fuel_used}}</ion-col>\n        <ion-col col-5 style="font-size:11px; font-weight: bold;">{{r.fuel_change}}</ion-col>\n        <ion-col col-3 style="font-size:11px; font-weight: bold;">{{r.distance}}</ion-col>\n      </ion-row>\n    <!-- </div> -->\n  </ion-card>\n</ion-content>'/*ion-inline-end:"/Users/admin/Desktop/ONGOING_PROJECTS/IONIC_APPS/route-trace-ionic/src/pages/fuel-consumption-report/fuel-consumption-report.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], FuelConsumptionReportPage);
    return FuelConsumptionReportPage;
}());

//# sourceMappingURL=fuel-consumption-report.js.map

/***/ }),

/***/ 518:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FuelConsumptionReportPageModule", function() { return FuelConsumptionReportPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__fuel_consumption_report__ = __webpack_require__(1022);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(364);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var FuelConsumptionReportPageModule = /** @class */ (function () {
    function FuelConsumptionReportPageModule() {
    }
    FuelConsumptionReportPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__fuel_consumption_report__["a" /* FuelConsumptionReportPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__fuel_consumption_report__["a" /* FuelConsumptionReportPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"]
            ],
        })
    ], FuelConsumptionReportPageModule);
    return FuelConsumptionReportPageModule;
}());

//# sourceMappingURL=fuel-consumption-report.module.js.map

/***/ })

});
//# sourceMappingURL=34.js.map